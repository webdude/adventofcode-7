<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Purple test</title>

        <!-- Styles -->
    </head>
    <body>
        <p>
            Input of the puzzle.
        </p>
        <div>
            <form action="/steps/solve" method="post">
            @csrf
                <textarea rows="4" cols="50" name="puzzle-input"></textarea>

                <br>
                <button type="submit">Get step order</button>
            </form>
        </div>
    </body>
</html>
