<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function(){
   return view('index');
});
Route::get('/puzzle-input', function () {
    return view('puzzle-input');
});
Route::get('/puzzle-input-part-2', function () {
    return view('puzzle-input-part-2');
});
Route::post(
    'steps/solve', 'StepsController@solve'
);
Route::post(
    'steps/solve-part-2', 'StepsController@solveStep2'
);
