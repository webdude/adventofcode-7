<?php

namespace App\Http\Controllers;

use App\Step;
use Illuminate\Http\Request;

class StepsController extends Controller
{

    /**
     * Keep all the steps with their parents
     * @var array
     */
    public $steps = [];

    /**
     * Keep steps with parent array size
     * @var array
     */
    public $parentSize = [];

    /**
     * Free worker pool. When a worker gets a step is excluded from this pool.
     * @var array
     */
    public $freeWorkerPool = ['worker0', 'worker1', 'worker2', 'worker3', 'worker4'];

    /**
     * Keep the link between workers and steps as well as when a step should be finished
     * @var array
     */
    public $workerStepMap = [];


    /**
     * Steps on progress so a step is not taken by another worker
     * @var array
     */
    public $stepsInProgress = [];

    /**
     * Solve the puzzle first part
     * @param Request $request
     * @return array|string
     */
    public function solve(Request $request)
    {
        $lines = $this->splitLines($request->get('puzzle-input'));

        foreach ($lines as $line) {

            if ($this->validLineLength($line)) {
                $this->process($line);
            } else {
                return 'Invalid Input';
            }
        }

        $stepOrder = [];
        $this->getOrder($this->steps, $this->parentSize, $stepOrder);

        return implode('', $stepOrder);
    }

    /**
     * @param Request $request
     * @return string The number of seconds to do the steps
     */
    public function solveStep2(Request $request)
    {
        $lines = $this->splitLines($request->get('puzzle-input'));

        foreach ($lines as $line) {

            if ($this->validLineLength($line)) {
                $this->process($line);
            } else {
                return 'Invalid Input';
            }
        }

        return $this->getTime();
    }

    /**
     * Compute the time to get the steps done
     * @return int
     */
    private function getTime()
    {
        $timer = 0;
        while (count($this->parentSize[0])) {
            //check if a step is done, remove from stacks and free worker
            foreach ($this->workerStepMap as $worker => $stepInProgress) {

                if ($stepInProgress['doneAt'] == $timer) {
                    $this->parentSize[0] = $this->removeElement($this->parentSize[0], $stepInProgress['name']);
                    $this->freeWorkerPool[] = $worker;
                    unset($this->workerStepMap[$worker]);

                    //shift steps that have a completed parent to lower parent lists and remove completed parent from their parent list
                    foreach ($this->steps as $stepName => $stepInfo) {
                        if (in_array($stepInProgress['name'], $this->steps[$stepName]['parents'])) {
                            $this->steps[$stepName]['parents'] = $this->removeElement($stepInfo['parents'], $stepInProgress['name']);
                            $this->parentSize[$stepInfo['size']] = $this->removeElement($this->parentSize[$stepInfo['size']], $stepName);
                            $this->steps[$stepName]['size']--;
                            $this->parentSize[$this->steps[$stepName]['size']][] = $stepName;
                        }
                    }
                }
            }

            if (count($this->freeWorkerPool) == 0) {
                $timer++;
                continue;
            }

            //get all available not in progress steps to workers
            $availableSteps = array_diff($this->parentSize[0], $this->stepsInProgress);
            foreach ($availableSteps as $availableStep) {
                $freeWorker = array_shift($this->freeWorkerPool);
                if ($freeWorker) {
                    $this->stepsInProgress[] = $availableStep;
                    $stepTimer = $timer + $this->getStepTime($availableStep);
                    $this->workerStepMap[$freeWorker] = ['doneAt' => $stepTimer, 'name' => $availableStep];
                }
            }

            if (count($this->parentSize[0]))
                $timer++;
        }

        return $timer;
    }

    /**
     * Get the order in witch the steps are solved
     * @param $steps
     * @param $parentSize
     * @param $stepOrder
     */
    private function getOrder(&$steps, &$parentSize, &$stepOrder)
    {
        while (count($parentSize[0])) {

            if (count($parentSize[0]) != 1)
                sort($parentSize[0]);

            $currentStep = array_shift($parentSize[0]);
            $stepOrder[] = $currentStep;
            $parentSize[0] = $this->removeElement($parentSize[0], $currentStep);
            unset($steps[$currentStep]);

            //shift steps that have a completed parent to lower parent lists and remove completed parent from their parent list
            foreach ($steps as $stepName => $stepInfo) {
                if (in_array($currentStep, $steps[$stepName]['parents'])) {
                    $steps[$stepName]['parents'] = $this->removeElement($stepInfo['parents'], $currentStep);
                    $parentSize[$stepInfo['size']] = $this->removeElement($parentSize[$stepInfo['size']], $stepName);
                    $steps[$stepName]['size']--;
                    $parentSize[$steps[$stepName]['size']][] = $stepName;
                }
            }

            $this->getOrder($steps, $parentSize, $stepOrder);
        }

        return;
    }

    /**
     * @param $input String with puzzle input
     * @return array of line steps
     */
    private function splitLines($input)
    {
        return explode(PHP_EOL, $input);
    }


    /**
     * Check if input line length is valid
     * @param $line Step line to be checked
     * @return bool Line is the correct length
     */
    private function validLineLength($line)
    {
        return strlen(trim($line)) === Step::LINE_LENGTH;
    }


    /**
     * Get the steps' name and parent name
     * @param $line Step line processed
     */
    private function process($line)
    {
        $parentName = substr($line, 5, 1);
        $stepName = substr($line, 36, 1);
        $this->addSteps($this->steps, $this->parentSize, $stepName, $parentName);
    }


    /**
     * Add steps to the controller container
     * @param $steps array Container to hold all step info
     * @param $parentSize
     * @param $stepName
     * @param $parentName
     */
    private function addSteps(&$steps, &$parentSize, $stepName, $parentName)
    {

        if (!array_key_exists($parentName, $steps)) {
            $steps[$parentName] = ['size' => 0, 'parents' => []];
            $parentSize[0][] = $parentName;
        }

        if (array_key_exists($stepName, $steps)) {
            if (!in_array($parentName, $steps[$stepName]['parents'])) {
                array_push($steps[$stepName]['parents'], $parentName);
                $size = $steps[$stepName]['size'];

                $parentSize[$size] = $this->removeElement($parentSize[$size], $stepName);
                $size++;
                $steps[$stepName]['size'] = $size;
                $parentSize[$size][] = $stepName;
            }
        } else {
            $steps[$stepName] = ['size' => 1, 'parents' => [$parentName]];
            $parentSize[1][] = $stepName;
        }
    }

    /**
     * Remove element from array by value
     * @param $array
     * @param $value
     * @return array
     */
    function removeElement($array, $value)
    {
        return array_diff($array, (is_array($value) ? $value : array($value)));
    }

    /**
     * Compute work time for a step. As per rule: "Each step takes 60 seconds plus an amount corresponding to its letter"
     * @param $stepName
     * @return int
     */
    function getStepTime($stepName)
    {
        return ord(strtoupper($stepName)) - ord('A') + 1 + 60;
    }
}
