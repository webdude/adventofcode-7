<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Step extends Model
{
    const LINE_LENGTH = 48;
}
